#!/bin/sh

if [ $# -lt 1 ]; then 
    echo "Missing parameter \$1 (bucket-name)"
    echo "Usage: cors.sh <bucket-name> [set]"
    exit 1
fi

cur_dir=`dirname $0`

file="unset.json"
if [ $# -eq 2 ] && [ "$2" = "set" ]; then
  file="set.json"
fi 

gsutil cors set "$(realpath $cur_dir)/$file" gs://$1
gsutil cors get gs://$1