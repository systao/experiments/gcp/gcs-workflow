const {Storage} = require('@google-cloud/storage');
const storage = new Storage();

const BUCKET_NAME = "pijam-parking";

async function getBucket(bucketName) {
    let bucket = storage.bucket(bucketName);
    console.log(`Got bucket reference: ${bucketName}`);
        
    const bucketExists = await bucket.exists(bucketName);
    console.log(`Bucket exists? ${bucketExists}`);
    
    if ( !bucketExists ) {
        bucket = await storage.createBucket(bucketName);
        console.log(`Bucket ${bucket.name} created.`);
    }
    
    return bucket;
}

async function getUrl(bucketName, fileName, config) {
    console.log(`Generating signed URL for ${bucketName}/${fileName} ; content-type=${config.contentType}`)    
	try {
        const bucket = await getBucket(bucketName || BUCKET_NAME);
        console.log(`Retrieved bucket: ${bucket}`);
        
        const file = bucket.file(fileName);
        console.log(`Got file reference: ${fileName}`);
        
        const url = await file.getSignedUrl(config);
        console.log(`Generated signed url: ${url}`);
        
        return url;
    }
    catch (e) {
        console.error(e);
    }
}

exports.getSignedUrl = (req, res) => {
    res.set('Access-Control-Allow-Origin', '*');
    res.set('Access-Control-Allow-Credentials', 'true');
    
    if (req.method === 'OPTIONS') {
        res.set('Access-Control-Allow-Methods', 'GET');
        res.set('Access-Control-Allow-Headers', 'Content-Type');
        res.set('Access-Control-Max-Age', '3600');
        res.status(204).send('');
        return;
    }
    
    if (req.method !== 'POST') {
        return res.status(405).end();
    }
  
    //@todo  check auth
    
    getUrl(req.body.bucket, req.body.filename, {
        action: 'write',
        method: 'POST',
        expires: Date.now() + 2*60*1000,
        contentType: req.body.contentType,
        version: 'v4',
    }).
    then(url => {
        if ( url ) {
        	res.set('Content-Type', 'application/json')
        	res.send({ url: url[0] });
        }
        else {
        	res.status(500).end();  
        }
    });
    
};
