#!/bin/sh

cur_dir=`dirname $0`
dist_dir=$(realpath "$cur_dir/dist") 

mkdir -p $dist_dir

echo "Copying resources to $dist_dir"

cp $(realpath "$cur_dir/package.json") $dist_dir
cp $(realpath "$cur_dir/src/index.js") $dist_dir

echo "Deploying cloud functions from $dist_dir"

gcloud functions deploy signed-url \
  --region=europe-west1 \
  --entry-point=getSignedUrl \
  --memory=128MB \
  --runtime=nodejs8 \
  --service-account=gcs-editor@pijam-app-reloaded.iam.gserviceaccount.com \
  --source="$dist_dir" \
  --stage-bucket=pijam-staging \
  --timeout=10 \
  --max-instances=1 \
  --trigger-http

 $cur_dir/cors/cors.sh pijam-parking set
 